/*!
 * @file SPI_VFD.cpp
 *
 * @mainpage Adafruit SPI VFD Library
 *
 * @section intro_sec Introduction
 *
 * Arduino Library for 20T202DA2JA SPI VFD (vacuum fluorescent display)
 *
 * @section author Author
 *
 * Written by Limor Fried/Ladyada for Adafruit Industries.
 *
 * @section license License
 *
 * BSD license, check license.txt for more information. All text above must be
 * included in any redistribution
 */

#include <string.h>
#include "SPI_VFD.h"

uint8_t spi_vfd_clock, spi_vfd_data, spi_vfd_strobe; // SPI interface

uint8_t spi_vfd_displayfunction;
uint8_t spi_vfd_displaycontrol;
uint8_t spi_vfd_displaymode;

uint8_t spi_vfd_initialized;

uint8_t spi_vfd_numlines, spi_vfd_currline;


// When the display powers up, it is configured as follows:
//
// 1. Display clear
// 2. Function set:
//    N = 1; 2-line display
//    BR1=BR0=0; (100% brightness)
// 3. Display on/off control:
//    D = 0; Display off
//    C = 0; Cursor off
//    B = 0; Blinking off
// 4. Entry mode set:
//    I/D = 1; Increment by 1
//    S = 0; No shift
//
// Note, however, that resetting the Arduino doesn't reset the LCD, so we
// can't assume that its in that state when a sketch starts (and the
// SPI_VFD constructor is called).

void spi_vfd_init(uint8_t data, uint8_t clock, uint8_t strobe,
                 uint8_t brightness) {
  spi_vfd_clock = clock;
  spi_vfd_data = data;
  spi_vfd_strobe = strobe;

  pinMode(spi_vfd_clock, OUTPUT);
  pinMode(spi_vfd_data, OUTPUT);
  pinMode(spi_vfd_strobe, OUTPUT);

  // normal state of these pins should be high. We must bring them high or the
  // first spi_vfd_command will not be captured by the display module.
  digitalWrite(spi_vfd_strobe, HIGH);
  digitalWrite(spi_vfd_clock, HIGH);

  spi_vfd_numlines = 2; // default to 2x20 display (SAMSUNG 20T202DA2JA)
  spi_vfd_currline = 0;

  // set number of lines
  if (spi_vfd_numlines > 1)
    spi_vfd_displayfunction = VFD_2LINE;
  else
    spi_vfd_displayfunction = VFD_1LINE;

  if (brightness > VFD_BRIGHTNESS25) // catch bad values
    brightness = VFD_BRIGHTNESS100;

  // set the brightness and push the linecount with VFD_SETFUNCTION
  spi_vfd_setBrightness(brightness);

  // Initialize to default text direction (for romance languages
  spi_vfd_displaymode = VFD_ENTRYLEFT | VFD_ENTRYSHIFTDECREMENT;
  // set the entry mode
  spi_vfd_command(VFD_ENTRYMODESET | spi_vfd_displaymode);

  spi_vfd_command(VFD_SETDDRAMADDR); // go to address 0

  // turn the display on with no cursor or blinking default
  spi_vfd_displaycontrol = VFD_DISPLAYON;
  spi_vfd_display();

  spi_vfd_clear();
  spi_vfd_home();
}

/********** high level spi_vfd_commands, for the user! */
void spi_vfd_setBrightness(uint8_t brightness) {
  // set the brightness (only if a valid value is passed
  if (brightness <= VFD_BRIGHTNESS25) {
    spi_vfd_displayfunction &= ~VFD_BRIGHTNESS25;
    spi_vfd_displayfunction |= brightness;

    spi_vfd_command(VFD_FUNCTIONSET | spi_vfd_displayfunction);
  }
}

uint8_t spi_vfd_getBrightness() {
  // get the brightness
  return spi_vfd_displayfunction & VFD_BRIGHTNESS25;
}

void spi_vfd_clear() {
  spi_vfd_command(VFD_CLEARDISPLAY); // clear display, set cursor position to zero
}

void spi_vfd_home() {
  spi_vfd_command(VFD_RETURNHOME); // set cursor position to zero
}

void spi_vfd_setCursor(uint8_t col, uint8_t row) {
  int row_offsets[] = {0x00, 0x40, 0x14, 0x54};
  if (row > spi_vfd_numlines) {
    row = spi_vfd_numlines - 1; // we count rows starting w/0
  }

  spi_vfd_command(VFD_SETDDRAMADDR | (col + row_offsets[row]));
}

// Turn the display on/off (quickly)
void spi_vfd_noDisplay() {
  spi_vfd_displaycontrol &= ~VFD_DISPLAYON;
  spi_vfd_command(VFD_DISPLAYCONTROL | spi_vfd_displaycontrol);
}

void spi_vfd_display() {
  spi_vfd_displaycontrol |= VFD_DISPLAYON;
  spi_vfd_command(VFD_DISPLAYCONTROL | spi_vfd_displaycontrol);
}

// Turns the underline cursor on/off
void spi_vfd_noCursor() {
  spi_vfd_displaycontrol &= ~VFD_CURSORON;
  spi_vfd_command(VFD_DISPLAYCONTROL | spi_vfd_displaycontrol);
}
void spi_vfd_cursor() {
  spi_vfd_displaycontrol |= VFD_CURSORON;
  spi_vfd_command(VFD_DISPLAYCONTROL | spi_vfd_displaycontrol);
}

// Turn on and off the blinking cursor
void spi_vfd_noBlink() {
  spi_vfd_displaycontrol &= ~VFD_BLINKON;
  spi_vfd_command(VFD_DISPLAYCONTROL | spi_vfd_displaycontrol);
}
void spi_vfd_blink() {
  spi_vfd_displaycontrol |= VFD_BLINKON;
  spi_vfd_command(VFD_DISPLAYCONTROL | spi_vfd_displaycontrol);
}

// These spi_vfd_commands scroll the display without changing the RAM
void spi_vfd_scrollDisplayLeft(void) {
  spi_vfd_command(VFD_CURSORSHIFT | VFD_DISPLAYMOVE | VFD_MOVELEFT);
}
void spi_vfd_scrollDisplayRight(void) {
  spi_vfd_command(VFD_CURSORSHIFT | VFD_DISPLAYMOVE | VFD_MOVERIGHT);
}

// This is for text that flows Left to Right
void spi_vfd_leftToRight(void) {
  spi_vfd_displaymode |= VFD_ENTRYLEFT;
  spi_vfd_command(VFD_ENTRYMODESET | spi_vfd_displaymode);
}

// This is for text that flows Right to Left
void spi_vfd_rightToLeft(void) {
  spi_vfd_displaymode &= ~VFD_ENTRYLEFT;
  spi_vfd_command(VFD_ENTRYMODESET | spi_vfd_displaymode);
}

// This will 'right justify' text from the cursor
void spi_vfd_autoscroll(void) {
  spi_vfd_displaymode |= VFD_ENTRYSHIFTINCREMENT;
  spi_vfd_command(VFD_ENTRYMODESET | spi_vfd_displaymode);
}

// This will 'left justify' text from the cursor
void spi_vfd_noAutoscroll(void) {
  spi_vfd_displaymode &= ~VFD_ENTRYSHIFTINCREMENT;
  spi_vfd_command(VFD_ENTRYMODESET | spi_vfd_displaymode);
}

// Allows us to fill the first 8 CGRAM locations
// with custom characters
void spi_vfd_createChar(uint8_t location, uint8_t charmap[]) {
  location &= 0x7; // we only have 8 locations 0-7
  spi_vfd_command(VFD_SETCGRAMADDR | (location << 3));
  for (int i = 0; i < 8; i++) {
    spi_vfd_write(charmap[i]);
  }
}

void spi_vfd_print(const char str[]) {
  for (uint16_t i = 0; i < strlen(str); i++) {
    spi_vfd_write(str[i]);
  }
}

void spi_vfd_print(char c)
{
  spi_vfd_write(c);
}

/*********** mid level spi_vfd_commands, for sending data/cmds, init */

void spi_vfd_command(uint8_t value) {
  digitalWrite(spi_vfd_strobe, LOW);
  spi_vfd_send(VFD_SPICOMMAND);
  spi_vfd_send(value);
  digitalWrite(spi_vfd_strobe, HIGH);
}

void spi_vfd_write(uint8_t value) {
  digitalWrite(spi_vfd_strobe, LOW);
  spi_vfd_send(VFD_SPIDATA);
  spi_vfd_send(value);
  digitalWrite(spi_vfd_strobe, HIGH);
}

/************ low level data pushing spi_vfd_commands **********/

// write spi data
inline void spi_vfd_send(uint8_t c) {
  int8_t i;

  digitalWrite(spi_vfd_clock, HIGH);

  for (i = 7; i >= 0; i--) {
    digitalWrite(spi_vfd_clock, LOW);

    if (c & _BV(i)) {
      digitalWrite(spi_vfd_data, HIGH);
    } else {
      digitalWrite(spi_vfd_data, LOW);
    }

    digitalWrite(spi_vfd_clock, HIGH);
  }
}
