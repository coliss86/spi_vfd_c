/*!
 * @file SPI_VFD.h
 */
#pragma once

#include "Arduino.h"

// commands
#define VFD_CLEARDISPLAY 0x01 //!< Clear display, set cursor position to zero
#define VFD_RETURNHOME 0x02   //!< Set cursor position to zero
#define VFD_ENTRYMODESET 0x04 //!< Sets the entry mode
#define VFD_DISPLAYCONTROL                                                     \
  0x08 //!< Controls display; does stuff like turning it off and on
#define VFD_CURSORSHIFT 0x10 //!< Lets you move the cursor
#define VFD_FUNCTIONSET 0x30 //!< Used to send the function to set the display
#define VFD_SETCGRAMADDR                                                       \
  0x40 //!< Used to set the CGRAM (character generator RAM)
#define VFD_SETDDRAMADDR 0x80 //!< Used to set the DDRAM (display data RAM)

// flags for display entry mode
#define VFD_ENTRYRIGHT 0x00 //!< Used to set the text to flow from right to left
#define VFD_ENTRYLEFT 0x02  //!< Used to set the text to flow from left to right
#define VFD_ENTRYSHIFTINCREMENT                                                \
  0x01 //!< Used to 'right justify' text from the cursor
#define VFD_ENTRYSHIFTDECREMENT                                                \
  0x00 //!< Used to 'left justify' text from the cursor

// flags for display on/off control
#define VFD_DISPLAYON 0x04  //!< Turns the display on
#define VFD_DISPLAYOFF 0x00 //!< Turns the display off
#define VFD_CURSORON 0x02   //!< Turns the cursor on
#define VFD_CURSOROFF 0x00  //!< Turns the cursor off
#define VFD_BLINKON 0x01    //!< Turns the blinking cursor on
#define VFD_BLINKOFF 0x00   //!< Turns the blinking cursor off

// flags for display/cursor shift
#define VFD_DISPLAYMOVE 0x08 //!< Flag for moving the display
#define VFD_CURSORMOVE 0x00  //!< Flag for moving the cursor
#define VFD_MOVERIGHT 0x04   //!< Flag for moving right
#define VFD_MOVELEFT 0x00    //!< Flag for moving left

// flags for function set
#define VFD_2LINE 0x08         //!< 8 bit mode
#define VFD_1LINE 0x00         //!< 4 bit mode
#define VFD_BRIGHTNESS25 0x03  //!< 25% brightness
#define VFD_BRIGHTNESS50 0x02  //!< 50% brightness
#define VFD_BRIGHTNESS75 0x01  //!< 75% brightness
#define VFD_BRIGHTNESS100 0x00 //!< 100% brightness

#define VFD_SPICOMMAND 0xF8 //!< Send command
#define VFD_SPIDATA 0xFA    //!< Send data

/*!
 * @brief SPI_VFD constructor
 * @param data Data pin
 * @param clock Clock pin
 * @param strobe Strobe pin
 * @param brightness Desired brightness (default 100%)
 */
void spi_vfd_init(uint8_t data, uint8_t clock, uint8_t strobe, uint8_t brightness);

/*!
 * @brief High-level command to clear the display
 */
void spi_vfd_clear();
/*!
 * @brief High-level command to set the cursor position to zero
 */
void spi_vfd_home();

/*!
 * @brief Sets the brightness
 * @param brightness Desired brightness level
 */
void spi_vfd_setBrightness(uint8_t brightness);
/*!
 * @brief Gets the brightness
 * @return Returns the brightness level
 */
uint8_t getBrightness();
/*!
 * @brief High-level command to turn the display off
 */
void spi_vfd_noDisplay();
/*!
 * @brief High-level command to turn the display on
 */
void spi_vfd_display();
/*!
 * @brief High-level command to turn the blinking cursor off
 */
void spi_vfd_noBlink();
/*!
 * @brief High-level command to turn the blinking cursor on
 */
void spi_vfd_blink();
/*!
 * @brief High-level command to turn the underline cursor off
 */
void spi_vfd_noCursor();
/*!
 * @brief High-level command to turn the underline cursor on
 */
void spi_vfd_cursor();
/*!
 * @brief High-level command to scroll the display left without changing the
 * RAM
 */
void spi_vfd_scrollDisplayLeft();
/*!
 * @brief High-level command to scroll the display right without changing the
 * RAM
 */
void spi_vfd_scrollDisplayRight();
/*!
 * @brief High-level command to make text flow left to right
 */
void spi_vfd_leftToRight();
/*!
 * @brief High-level command to make text flow right to left
 */
void spi_vfd_rightToLeft();
/*!
 * @brief High-level command to 'right justify' text from the cursor
 */
void spi_vfd_autoscroll();
/*!
 * @brief High-level command to 'left justify' text from the cursor
 */
void spi_vfd_noAutoscroll();

/*!
 * @brief High-level command that creates a custom character in CGRAM
 * @param location Location in cgram to fill
 * @param charmap[] Character map to use
 */
void spi_vfd_createChar(uint8_t, uint8_t[]);
/*!
 * @brief High-level command that sets the location of the cursor
 * @param col Column to set the cursor in
 * @param row Row to set the cursor in
 */
void spi_vfd_setCursor(uint8_t, uint8_t);

/*!
 * @brief Mid-level command that sends data to the display
 * @param value Data to send to the display
 */
void spi_vfd_write(uint8_t);

/*!
 * @brief Sends command to display
 * @param value Command to send
 */
void spi_vfd_command(uint8_t);

void spi_vfd_print(const char str[]);
void spi_vfd_print(char c);

void spi_vfd_send(uint8_t data);
